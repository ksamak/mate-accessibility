alias mate-accessibility-configure="python /usr/lib/python2.7/mate-accessibility/tweak_desktop.py"

if [ -d .bash_aliases.d ]; then
  for i in .bash_aliases.d/*; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi
