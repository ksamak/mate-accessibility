#!/usr/bin/python
from gi.repository import Caja, GObject
import os
import subprocess
import urllib
import gettext

class OcrizeExtension(GObject.GObject, Caja.MenuProvider):
    def __init__(self):
        es = gettext.translation('mate-accessibility', fallback=True)
        es.install()

    def menu_activate_cb(self, menu, files):
        files_names = []
        for fil in files:
            filename = urllib.unquote(fil.get_uri()[7:])
            files_names.append(filename)
        path = os.path.split(files_names[0])[0]
        arguments = ["ocrizer", "-d", path]
        for i in files_names:
            arguments.append(i)
        subprocess.call(arguments)

    def check_extensions(self, files):
        extensions = '.pdf', '.png', '.jpg', '.jpeg', '.gif', '.bmp', '.tiff', '.dcx', '.jbig2'
        for fil in files:
            if not fil.get_name().lower().endswith(extensions):
                return False
        return True

    def get_file_items(self, window, files):
        if not self.check_extensions(files):
            return
        item = Caja.MenuItem(
            name="Ocrize",
            label=_("Launch Optical Character Recognition")
        )
        item.connect('activate', self.menu_activate_cb, files)
        return [item]
