#!/bin/sh

CUR_VOLUME=$(pacmd list-sinks | grep front-left | grep -v channel | cut -d " " -f3)
CUR_SINK=$(pacmd list-sinks | grep index | cut -d " " -f 5)
paplay --volume $* < /dev/null
pacmd set-sink-volume $CUR_SINK $CUR_VOLUME
