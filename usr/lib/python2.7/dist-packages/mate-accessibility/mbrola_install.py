#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os

from base_install import Accessibility_Installer

class Mbrola_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall\n\tuninstall" % sys.argv[0]

    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "mate-accessibility-mbrola"

    def install_v0_2(self):
        speechd_conf = "/etc/speech-dispatcher/speechd.conf"
        commands_to_execute = [
            'sed -i "s/^\#\(AddModule .espeak-mbrola-generic.\)/\\1/" %s' % speechd_conf,
            'sed -i "s/^DefaultModule\ espeak$/DefaultModule espeak-mbrola-generic/" %s' % speechd_conf
        ]
        self.execute_commands(commands_to_execute)

    def uninstall_v0_2(self):
        speechd_conf = "/etc/speech-dispatcher/speechd.conf"
        commands_to_execute = [
            'sed -i "s/^\(AddModule .espeak-mbrola-generic.\)/#\\1/" %s' % speechd_conf,
            'sed -i "s/^DefaultModule\ espeak-mbrola-generic/DefaultModule espeak/" %s' % speechd_conf
        ]
        self.execute_commands(commands_to_execute)

    def install_v0_3(self):
        self.install_v0_2()

    def uninstall_v0_3(self):
        self.uninstall_v0_2()

    def install_v0_5(self):
        self.install_v0_3()

    def uninstall_v0_5(self):
        self.uninstall_v0_3()

    def install_v0_6(self):
        self.install_v0_3()

    def uninstall_v0_6(self):
        self.uninstall_v0_3()

    def install_v0_7(self):
        self.install_v0_3()

    def uninstall_v0_7(self):
        self.uninstall_v0_3()

    def install_v0_8(self):
        self.install_v0_3()

    def uninstall_v0_8(self):
        self.uninstall_v0_3()

if __name__ == "__main__":
    inst = Mbrola_Installer()
    inst.launch(sys.argv)
