#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import ConfigParser
import hashlib
import shutil
from distutils.version import LooseVersion

from base_install import Accessibility_Installer

class Compiz_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall :\t install mate-a11y setup\n\tconfigure :\t alias ot install\n\tuninstall :\t uninstall mate-a11y setup" % sys.argv[0]
    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "mate-accessibility-compiz"

    def install_v0_1(self):
        self.enable_compiz_mate_accessibility_profile()

    def uninstall_v0_1(self):
        self.disable_compiz_mate_accessibility_profile()

    def install_v0_2(self):
        self.enable_compiz_mate_accessibility_profile()

    def uninstall_v0_2(selfh):
        self.disable_compiz_mate_accessibility_profile()

    def install_v0_3(self):
        self.install_v0_2()

    def uninstall_v0_3(self):
        self.uninstall_v0_2()

    def install_v0_5(self):
        self.install_v0_3()

    def uninstall_v0_5(self):
        self.uninstall_v0_3()

    def install_v0_6(self):
        self.enable_compiz_mate_accessibility_profile()

    def uninstall_v0_6(self):
        self.disable_compiz_mate_accessibility_profile()

    def install_v0_7(self):
        self.install_v0_6()

    def uninstall_v0_7(self):
        self.uninstall_v0_6()

    def install_v0_8(self):
        self.install_v0_6()

    def uninstall_v0_8(self):
        self.uninstall_v0_6()

    def upgrade_v0_8(self):
        # if the settings are yet installed
        if LooseVersion(self.last_configured_version) > LooseVersion('0.5.2'):
            print "not upgrading compiz configuration, version already too high (>0.5.2)"
            return
        parser = ConfigParser.ConfigParser()
        for user in self.get_non_system_users():
            username = self.get_username(user)
            compiz_config_file = '/home/%s/.config/compiz-1/compizconfig/config' %username
            if not os.path.exists(compiz_config_file):
                continue
            parser.read(compiz_config_file)
            parser.set("general", "backend", "ini")
            with open(compiz_config_file, "w") as fp:
                parser.write(fp)

    def enable_compiz_mate_accessibility_profile(self):
        parser = ConfigParser.ConfigParser()
        parser.read("/etc/compizconfig/config")
        if not parser.has_section("general"):
            parser.add_section("general")
        parser.set("general", "backend", "gsettings")
        parser.set("general", "integration", "false")
        parser.set("general", "profile", "mate-accessibility")
        with open("/etc/compizconfig/config", "w") as fp:
            parser.write(fp)
        print "modified default profile for compiz to mate a11y"

    def disable_compiz_mate_accessibility_profile(self):
        parser = ConfigParser.ConfigParser()
        parser.read("/etc/compizconfig/config")
        parser.remove_option("general", "profile")
        with open("/etc/compizconfig/config", "w") as fp:
            parser.write(fp)
        print "removed mate a11y's profile in compiz config"

if __name__ == "__main__":
    inst = Compiz_Installer()
    inst.launch(sys.argv)
