#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
from distutils.version import LooseVersion

from base_install import Accessibility_Installer

class Core_Accessibility_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall :\t install mate-a11y setup\n\tuninstall :\t uninstall mate-a11y setup" % sys.argv[0]
    def __init__(self):
        self.pkg_name = "mate-accessibility-common" # to get changelog
        self.version = None
        self.available_desktop_shortcuts = ["firefox-esr.desktop", "gpk-update-viewer_.desktop", "gpk-application_.desktop", "keyboard_shortcut_list.desktop", "voxiweb.desktop"]

    def setup_common_aliases(self):
        user = self.get_first_system_user()
        if not user:
            return
        commands_to_execute = [
                'cat /etc/skel/.bash_aliases | grep -q -f - /home/%s/.bash_aliases || cp /etc/skel/.bash_aliases /home/%s/.bash_aliases' %(user, user) # TODO change that to per line base. and chmod to user...
                ]
        self.execute_commands(commands_to_execute)

    def teardown_common_aliases(self):
        user = self.get_first_system_user()
        commands_to_execute = [
                'rm -f /home/%s/.bash_aliases' %user,
                'rm -rf /home/%s/.bash_aliases.d' %user
                ]
        self.execute_commands(commands_to_execute)

    def setup_desktop(self):
        desktop_folder = self.get_first_user_desktop()
        iceweasel_shortcut = os.path.join(desktop_folder, "iceweasel.desktop")
        # if Iceweasel desktop shortcut exists, we should remove the new Firefox esr icon
        if os.path.exists(iceweasel_shortcut):
            self.available_desktop_shortcuts.remove("firefox-esr.desktop")
        current_user = self.get_first_system_user()
        self.add_shortcuts_to_desktop(current_user, desktop_folder)

    def install_v0_1(self):
        self.setup_common_aliases()

    def uninstall_v0_1(self):
        self.teardown_common_aliases()

    def install_v0_2(self):
        self.setup_common_aliases()
        self.setup_desktop()

    def uninstall_v0_2(self):
        self.teardown_common_aliases()

    def install_v0_3(self):
        self.install_v0_2()

    def uninstall_v0_3(self):
        self.uninstall_v0_2()

    def install_v0_5(self):
        self.install_v0_3()

    def uninstall_v0_5(self):
        self.uninstall_v0_3()

    def install_v0_6(self):
        self.install_v0_3()

    def uninstall_v0_6(self):
        self.uninstall_v0_3()

    def install_v0_7(self):
        self.setup_desktop()

    def uninstall_v0_7(self):
        pass

    def install_v0_8(self):
        self.install_v0_7()

    def uninstall_v0_8(self):
        pass

    def upgrade_v0_8(self):
        # if the settings are yet installed
        if LooseVersion(self.last_configured_version) >= LooseVersion('0.7.0'):
            return
        self.uninstall_v0_6()

if __name__ == "__main__":
    inst = Core_Accessibility_Installer()
    inst.launch(sys.argv)
