#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import shutil
import logging

from base_install import Accessibility_Installer

class Orca_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall :\t install orca setup\n\tuninstall :\t uninstall orca setup" % sys.argv[0]
    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "mate-accessibility-orca"

    def install_v0_1(self):
        self.setup_orca_shortcuts()

    def uninstall_v0_1(self):
        self.teardown_orca_shortcuts()

    def install_v0_2(self):
        self.setup_orca_shortcuts()
        #self.setup_desktop() # TODO make liste racourcis clavier lecteur ecran

    def uninstall_v0_2(self):
        self.teardown_orca_shortcuts()
        #self.teardown_desktop()

    def install_v0_3(self):
        self.install_v0_2()

    def uninstall_v0_3(self):
        self.uninstall_v0_2()

    def install_v0_5(self):
        self.install_v0_3()

    def uninstall_v0_5(self):
        self.uninstall_v0_3()

    def install_v0_6(self):
        self.install_v0_3()

    def uninstall_v0_6(self):
        self.uninstall_v0_3()

    def install_v0_7(self):
        self.install_v0_3()

    def uninstall_v0_7(self):
        self.uninstall_v0_3()

    def install_v0_8(self):
        self.install_v0_3()

    def uninstall_v0_8(self):
        self.uninstall_v0_3()

    def setup_orca_shortcuts(self):
        current_user = self.get_first_system_user()
        if not current_user:
            return
        conf_samples = "/etc/skel/.local/share/orca/"
        user_conf = os.path.join(self.get_first_user_home(), ".local/share/orca/")
	if os.path.exists(user_conf):
            return
        self.execute_commands([
            'cp -r %s %s' %(conf_samples, user_conf),
            'chown -R %s:%s %s' %(self.get_first_system_user(), self.get_first_system_user(), user_conf)
            ])
        # TODO make auto merging attempt, or inject profile (or not cause gsettings)
        # self.json_merge_tree(user_conf, conf_samples, "--override" in sys.argv, formatt="json")

    def teardown_orca_shortcuts(self):
        pass

if __name__ == "__main__":
    inst = Orca_Installer()
    inst.launch(sys.argv)
